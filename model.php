<?php
session_start();

$host = 'localhost';
$user = 'root';
$pass = '';
$db = 'iak';

$l = mysqli_connect($host, $user, $pass, $db);
mysqli_query($l, 'SET CHARACTER SET UTF8');

function model_getUser($username, $pw) {

    global $l;

    $query = 'SELECT id, password FROM iak_users WHERE username=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 's', $username);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $password);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    if ($password === $pw) {
        return true;
    }

    return false;
}

function model_getConcerts() {

    global $l;

    $query = "SELECT id, date, name, address, comment, image FROM  `iak_concert` ORDER BY  `id` DESC;";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $time, $name, $address, $comment, $image);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'time' => $time,
            'name' => $name,
            'address' => $address,
            'comment' => $comment,
            'image' => $image,
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function model_addConcert($rname, $date, $time, $raddress, $comment, $image) {

    global $l;

    $query = "INSERT INTO `iak_concert`(`id`, `name`, `date`, `time`, `address`, `comment`, `image`) VALUES ('','$rname', '$date', '$time','$raddress', '$comment','$image')";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;
}

function model_editConcert($id) {

    global $l;

    $query = 'EDIT FROM iak_concert WHERE Id=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    $edited = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return $edited;
}

function model_deleteConcert($id) {

    global $l;

    $query = 'DELETE FROM iak_concert WHERE Id=? LIMIT 1';
    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    $deleted = mysqli_stmt_affected_rows($stmt);
    mysqli_stmt_close($stmt);

    return $deleted;
}

function model_addUser($username, $password1, $name) {

    global $l;

    $query = "INSERT INTO iak_users(username, password, name) VALUES ('$username', '$password1', '$name')";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;

}

function model_getKanals() {
    global $l;

    $query = "SELECT id,bron FROM iak_kanal";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $id, $bron);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'id' => $id,
            'bron' => $bron
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

function model_addReservation($bname,$bdate,$barrive,$blength,$bnumber,$brname,$bron,$tel,$comment) {
    global $l;

    $query = "INSERT INTO `iak_reservation`(`id`, `rid`, `date`, `time`, `duration`, `number`, `name`, `kid`, `tel`, `comments`, `bdate`) VALUES ('','$bname','$bdate','$barrive','$blength','$bnumber','$brname','$bron','$tel','$comment',NOW())";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    $id = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    return $id;
}

function model_getReservations() {
    global $l;

    $query = "SELECT iak_concert.name as rname, iak_reservation.name as bname, iak_reservation.bdate FROM iak_concert INNER JOIN iak_reservation ON iak_concert.id=iak_reservation.rid ORDER BY iak_reservation.bdate";

    $stmt = mysqli_prepare($l, $query);
    if (mysqli_error($l)) {
        echo mysqli_error($l);
        exit;
    }

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $rname, $bname, $bdate);

    $rows = array();
    while (mysqli_stmt_fetch($stmt)) {
        $rows[] = array(
            'rname' => $rname,
            'bname' => $bname,
            'bdate' => $bdate
        );
    }

    mysqli_stmt_close($stmt);

    return $rows;
}

