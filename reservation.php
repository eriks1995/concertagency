<?php
include("header.php");
?>

    <div class="reservationform">
        <div class="text">
            <p>Please fill all fields and save. Reservations are displayed at the bottom.
            <p>To registrate concert klick <a href="./concert.php">here</a>.
            <p>To return klick <a href="./secure.php">here</a>.
        </div>

        <form method="POST" action="add-reservation.php">
            Concert's name: <br>
            <select name="bname">
                <?php foreach (model_getConcerts() as $row):?>
                    <option value="<?= $row["id"]?>"><?= $row["name"]?></option>';
                <?php endforeach; ?>
            </select><br>
            Date: <br>
            <input type="date" name="bdate"/><br>
            Time: <br>
            <input type="time" name="barrive"/><br>
            Duration (hours): <br>
            <input type="number" min="0" step="0.5" name="blength"/><br>
            Number of persons: <br>
            <input type="number" min="1" max="50" step="1" name="bnumber"/><br>
            Your Name: <br>
            <input pattern=".{2,50}" type="text" name="brname"/><br>
            Reservation:
            <select name="bron"><br>
                <?php foreach (model_getKanals() as $row):?>
                    <option value="<?= $row["id"]?>"><?= $row["bron"]?></option>';
                <?php endforeach; ?>
            </select><br>
            Your Contacts: <br>
            <input pattern=".{5,10}" type="text" name="btel"/><br>
            Comment: <br>
            <textarea name="bcom" maxlength="460" rows="10" cols="50"></textarea><br>
            <input type="submit" name="rsubmit" value="Save"/>
        </form>
    </div>
    <hr>
    <div class="added-reservations">
        <div class="entered">
            <p><b>All reservations sorted by date and time.</b>
        </div>
        <?php
        if (isset($_SESSION["username"])):
            foreach (model_getReservations() as $row):?>
            Concert: <?= $row["rname"]?><br>Name: <?= $row["bname"]?><br>Reservation: <?= $row["bdate"]?><br><br>
        <?php endforeach;
        endif; ?>
    </div>

<?php
include("footer.php");
?>