<?php

require "model.php";

if (isset($_POST['rsubmit'])) {

    $rname = $_POST['name'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $raddress = $_POST['address'];
    $comment = $_POST['comment'];
    $image = $_POST['image'];

    model_addConcert($rname, $date, $time, $raddress, $comment, $image);
}
header("Location: concert.php");