<?php

require "model.php";

$rid = $_GET["id"];

$deleted = model_deleteConcert($rid);

if($deleted){
    header("location: concert.php");
} else {
    echo "Concert, id " . $rid . " not found!";
}