<?php
include("header.php");
?>
    <div class="insert">
        <div class="text">
            <p>To insert the concert please fill all fields and save. Inserted concerts are displayed at the bottom.
            <p>To make reservation klick <a href="./reservation.php">here</a>.
            <p>To return klick <a href="./secure.php">here</a>.
        </div>
        <form method="POST" action="add-concerts.php">
            Concert's name: <br><input pattern=".{3,30}" type="text" name="name" required title="minimum 3 characters"/><br>
            Date: <br>
            <input type="date" name="date"/><br>
            Time: <br>
            <input type="time" name="time"/><br>
            Place: <br><input pattern=".{5,100}" type="text" name="address" title="minimum 5 characters"/><br>
            Comment: <br>
            <textarea name="comment" maxlength="460" rows="10" cols="50"></textarea><br>
            Image: <br><input type="file" name="image"/><br>
            <input name="rsubmit" type="submit" value="Save"/>
        </form>
    </div>
    <hr>
    <div class="look">
        <div class="inserted">
            <p><b>All concerts sorted by name and address.</b>
        </div>
        <table>
            <?php foreach (model_getConcerts() as $row):?>
                <tr>
                    <td><a href="edit-concerts.php?id=<?= $row["id"]?>">Edit  </a></td>
                    <td><a href="delete-concerts.php?id=<?= $row["id"]?>">Delete  </a></td>
                    <td class='series'>
                        <?= $row["name"]?><br>
                        <?= date('l jS \of F Y h:i A')?><br>
                        <?= $row["address"]?><br>
                        <?= $row["comment"]?><br>
                    </td>
                    <td>
                        <img src="images/<?= $row["image"]?>" width=430 height=200>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>

<?php
include("footer.php");
?>